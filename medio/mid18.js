/**
 * Filter/map
 * 
 * Javascript proporciona dos mecanismos muy útiles para trabajar con arrays
 *    - filter, que genera un nuevo array con un subconjunto de los elementos
 * del array original
 *    - map, que genera un nuevo array con el mismo número de elementos que 
 * el array original, pero transformados
 * 
 * https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/filter
 * https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/map
 * 
 *  
 * En este ejercicio vamos a rehacer algunos ejercicios que ya hicimos, pero usando esta técnica
 * 
 */

// 1) devolver un array con los números pares del array original
//const numbers = [10, 20, 30, 40];

console.log(`EJERCICIO 1`);

const numbers = [1,2,3,4,5];

const pares = numbers.filter(function(num)
{
    return num % 2 === 0;
});

console.log(pares);


// 2) devolver un array con los cuadrados del array original

console.log(`EJERCICIO 2`);

const values = [1,2,3,4];

const dobles = values.map(function(num)
{
    return Math.pow(num, 2);
});

console.log(dobles);


// 3) devolver un array con los cuadrados de los números pares que sean mayores que dos

console.log(`EJERCICIO 3`);

const values2 = [1,2,3,4];  // [4, 16]
let mayorQue = 2

const pares2 = numbers.filter((num, mayorQue) => num % 2 === 0 && num >= mayorQue)

const doblesSupDos = pares2.map((num) => Math.pow(num, 2))

console.log(doblesSupDos);

// 4) devolver un array con las notas de los estudiantes

console.log(`EJERCICIO 4`);

const students = [
    {
        name: 'Jose',
        grades: [1, 2, 3]
    },
    {
        name: 'María',
        grades: [10, 9, 8]
    }
]

medias = students.map(function (estudiantes)
{
    return calculamedia(estudiantes);
})

console.log(medias);

// 5) Devolver un array con los nombres de los estudiantes aprobados

console.log(`EJERCICIO 5`);

let notaCorte = 0

const aprobados = students.filter((estudiantes) => calculamedia(estudiantes) >= notaCorte) // Forma simplificada de llamar a una función, esto se usa cuando solo tiene una linea

console.log(aprobados);






function calculamedia(estudiantes) {

    let media = 0;

    for (notas of estudiantes.grades) {
        media += notas;
    }

    return media / estudiantes.grades.length;
}

