/**
 * Arrays y funciones
 * 
 * Crea una función que reciba un array y busque en él
 * cuál es la secuencia de números positivos más larga. 
 * La función devolverá los índices de inicio y fin de 
 * dicha secuencia. Ejemplos:
 * 
 * 
 * [1,2,3,4,5,6] -> devuelve 0 y 5
 * [-1,-1,0,2,-1] -> devuelve 2 y 3
 * [1,1,1,-1,1,1,1,1,1] -> devuelve 4 y 8
 * 
 * Podemos hacer el ejercicio en dos fases:
 * 
 *    a) Supongamos que hay una sola secuencia de números positivos 
 *    b) Supongamos que hay un número indefinido de secuencias
 * 
 * 
 * ¿Cómo devuelvce una función varios valores? Hasta ahora las funciones
 * que hemos implementado devolvían un valor que podía ser una cadena o un número,
 * pero nada impide devolver, por ejemplo, un array. Así, para este ejercicio, 
 * nuestra función devolverá un array de dos posiciones, la primera representa
 * el inicio de la secuencia positiva y el segundo, el final. Ejemplo
 * 
 *   [-1,-1,0,2,-1] -> devuelve [2 ,3]
 * 
 */



function calculaDevolucion(cadenaNumeros) {
    
    //Variables de inicializacion
    let inicial = -1
    let final = 0
    let pasos = 0
    let posiciones = [-1,-1,-1]

    for (i = 0; i < cadenaNumeros.length; i++)
    {
        if (cadenaNumeros[i] >=0)
        {
            if (inicial < 0)
            {
                inicial = i
            }
            final = i
            pasos++
        }
        else
        {
            if(posiciones[2] < pasos)
            {
                posiciones = [inicial,final,pasos]
            }

            inicial = -1
            final = 0
            pasos = 0
        }
        
    }
    if(posiciones[2] < pasos)
    {
        posiciones = [inicial,final,pasos]    
    }
    posiciones.pop()
    return posiciones
}

let numeros = [1,1,1,1,-1,1,1,1,1,1]


console.log(calculaDevolucion(numeros));