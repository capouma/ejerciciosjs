/**
 * Estamos programando una página de una inmobiliaria. 
 * 
 * Los inmuebles disponibles se representan como un array de objetos
 * con la siguiente estructura:
 * 
 * {
 *    id: 1
 *    name: 'Piso en el centro',
 *    rooms: 2,
 *    metres: 100,
 *    price: 400
 * }
 *  
 * Escribe una función para filtrar los inmuebles que cumplan una serie
 * de condiciones. Dicha función recibirá el array de inmuebles,  el 
 * número mínimo de habitaciones, el número mínimo de metros y el
 * precio máximo.
 * 
 * Si no desea filtrar por alguna de las condiciones, el valor del parámetro
 * será -1
 * 
 */

let houses = [
{
    id: 1,
    name: 'Piso en el centro',
    rooms: 2,
    metres: 80,
    price: 100000
},
{
    id: 2,
    name: 'Piso calle X',
    rooms: 3,
    metres: 90,
    price: 150000
},   
{
    id: 3,
    name: 'Casa en la playa',
    rooms: 4,
    metres: 120,
    price: 200000
},
]


function buscaPiso(houses, minRooms, minMetres, maxPrice) {

    let habitaciones = piso => piso.rooms
    let metrosPiso = piso => piso.rooms >= minRooms
    let precioMax = piso => (piso.price <= maxPrice || maxPrice === -1)

            //FORMA LARGA
    //const pisoFiltrado = houses.filter(piso => piso.rooms >= minRooms && piso.metres >= minMetres && (piso.price <= maxPrice || maxPrice === -1))


    const pisoFiltrado = houses
        .filter(habitaciones)
        .filter(metrosPiso)
        .filter(precioMax)

    return pisoFiltrado.map (nombrePiso => nombrePiso.name)

}

console.log(buscaPiso(houses, 2, 80, 100000));