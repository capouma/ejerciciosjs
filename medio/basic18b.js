/**
 * Repaso
 * 
 * En este ejercicio hará falta usar todo lo aprendido hasta ahora
 * El objetivo es escribir en pantalla una ficha de estudiante
 * con el siguiente aspecto:
 * 
 *     **********************
 *     * Manolo Pérez Gómez *
 *     *                    *
 *     *        8.3         *
 *     *     (APROBADO)     *
 *     **********************
 * 
 * Notas:
 *    - notad que tanto la nota numérica como textual están centradas
 *    - vuestro código debe funcionar para cualquier nombre y apellidos,
 *      independientemente de su longitud
 *    - podéis usar la función toFixed
 *    - podéis hacer uso de la función `repeat`: 
 *      https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/String/repeat
 */


function calculaMasGrande(nombreCompleto, mediaEstudiante)
{
    let referencia = []
    for(let i = 0; i < nombreCompleto.length; i++)
    {
        if (nombreCompleto[i].length < mediaEstudiante[i].gradeText.length)
        {
            referencia[i] = mediaEstudiante[i].gradeText.length + 2
        }
        else
        {
            referencia[i] = nombreCompleto[i].length + 2
        }
    }

    return referencia
}

function calculaPref (referenciaTam, nombre, numMedia, media)
{
    let calculoPre = []
    for (let i = 0; i < nombre.length; i++)
    {
        calculoPre.push( ' '.repeat((referenciaTam[i] / 2) - (parseInt(nombre[i]/ 2))))
        calculoPre.push( ' '.repeat((referenciaTam[i] / 2) - (parseInt(numMedia[i]/ 2))))
        calculoPre.push( ' '.repeat((referenciaTam[i] / 2) - (parseInt(media[i]/ 2))))
    }
    return calculoPre
}

const students =
[
    {
        id :1,
        name: 'Manolo',
        surname: 'Pérez',
        surname2: 'Rubio',
        avgGrade: 7,
        gradeText: '!!!!!NOTABLEEEEEE!!!!!'
    },
    {
        id :2,
        name: 'Josito',
        surname: 'T',
        surname2: 'P',
        avgGrade: 4,
        gradeText: '!!!!!SUSPENSOOOO!!!!!'
    },
    {
        id :3,
        name: 'Marta',
        surname: 'Pérez',
        surname2: 'Rubio',
        avgGrade: 5,
        gradeText: 'APROBADO'
    },

]

const searchFullNames = student => `${student.name} ${student.surname} ${student.surname2}`
const searchAvgGrade = student => student.avgGrade
const searchGradeText = student => student.gradeText

const calculateFullNameText = fullName => fullName.length


const fullName = students.map(searchFullNames)
const avgGrade = students.map(searchAvgGrade)
const gradeText = students.map(searchGradeText)

const grade = students.map(student => student.avgGrade)
const referenceLength = calculaMasGrande(fullName, students)

const fullNameTextLength = fullName.map(calculateFullNameText)
const gradeTextLength = calGradeTextLength (students)
const avgGradeLength = calAvgGradeLength (students)

const preFix = calculaPref (referenceLength, fullNameTextLength, avgGradeLength, gradeTextLength)


// console.log(fullName);
// console.log(referenceLength);
console.log(grade);


console.log('*'.repeat(referenceLength[0]));
console.log(`* ${fullName[0]} *`)
console.log(`*${' '.repeat(referenceLength[0]-2)}*`)
console.log(`*${preFix[1]}${grade}*`)
console.log(fullNameTextLength);
console.log(gradeTextLength);
console.log(preFix);


