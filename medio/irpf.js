
salarioBruto = 22000;

totalPrimerTramo = 12450
totalSegundoTramo = 20200 - 12450
totalTercerTramo = 35200 - 20200
totalCuartoTramo = 60000 - 35200

UMBRAL_TRAMO_1 = 12450
UMBRAL_TRAMO_2 = 20200
UMBRAL_TRAMO_3 = 35200
UMBRAL_TRAMO_4 = 60000

pendienteDeDeducir = salarioBruto
retencion = 0

let controlDeTramos = []
let resultado = []
let retencionSuma = 0

//Controlamos los tramos por los que se debe pasar
if (salarioBruto >= 60000)
{
    controlDeTramos = [0.45, 0.37, 0.3, 0.24, 0.19]
    controlUmbralTramo = [60000, 35200, 20200, 12450]
}
else if (salarioBruto >= 35200)
{
    controlDeTramos = [0.37, 0.3, 0.24, 0.19]
    controlUmbralTramo = [35200, 20200, 12450]
}
else if (salarioBruto >= 20200)
{
    controlDeTramos = [0.3, 0.24, 0.19]
    controlUmbralTramo = [20200, 12450]
}
else if (salarioBruto >= 20200)
{
    controlDeTramos = [0.24, 0.19]
    controlUmbralTramo = [20200, 12450]
}
else if (salarioBruto >= 12450)
{
    controlDeTramos = [0.19]
    controlUmbralTramo = [12450]
}

for (let i = 0; i < controlDeTramos.length; i++)
{
    let umbralTramo = controlUmbralTramo[i]
    let retencion = controlDeTramos[i]
    let retencionYPendienteDeDeducir = []

    retencionYPendienteDeDeducir= retenciones (pendienteDeDeducir, umbralTramo, retencion)

    retencionSuma += retencionYPendienteDeDeducir[0]
    pendienteDeDeducir = retencionYPendienteDeDeducir[1]
}
retencionTotal = retencionSuma + pendienteDeDeducir * 0.19
console.log(`El total de retencion para ${salarioBruto} es ${retencionTotal}`);

function retenciones (pendienteDeDeducir, umbralTramo, retencion)
{
    let calcularetencionYPendienteDeDeducir =[]
    if (pendienteDeDeducir >= umbralTramo)
    {
        calcularetencionYPendienteDeDeducir.push((pendienteDeDeducir - umbralTramo) * retencion)
        calcularetencionYPendienteDeDeducir.push(umbralTramo)
        console.log(`Retención: ${calcularetencionYPendienteDeDeducir[0]}   - pendiente: ${calcularetencionYPendienteDeDeducir[1]}`)
        return calcularetencionYPendienteDeDeducir
    }
}