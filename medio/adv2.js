/**
 * Fechas
 * 
 * Escribe una función que devuelva cuántos días
 * quedan hasta la fecha pasada como parámetro. Esta
 * última se pasa en milisegundos.
 * 
 */
const getPendingDays = timestamp => {
const now = Date.now()

// calculo la diferencia entre ambas fechas,
// primero en milisegundos y después lo pasamos a días
const pendingDays = (timestamp - now) / 1000 / 3600 / 24

return parseInt(pendingDays)
}

const targetDate = (new Date(2021, 3, 25)).getTime()

const pendingDays = getPendingDays(targetDate)

console.log(`Los días pendientes son: ${pendingDays} días`)