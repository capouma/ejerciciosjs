/**
 * En los próximos ejercicios veremos cómo hacer peticiones de red,
 * es decir, pedirle a `NodeJS` que descargue de Internet el contenido
 * de una `URL`. Esta descarga no será inmediata, así que estudiaremos
 * un mecanismo para indicarle cuál es el código que tiene que ejecutar una 
 * vez finalizada.
 * 
 * Antes de eso, implementemos una serie de funciones auxiliares que, de paso,
 * nos sirven de repaso de cómo iterar `arrays`, bien con `for` tradicional
 * o con `map/filter`
 * 
 * El ejercicio consiste en implementar dos funciones que realizan estadísticas
 * sobre datos reales: las banderas azules en ayuntamientos gallegos. En esta
 * primera aproximación leeremos los datos de disco. En ejercicios posteriores
 * lo reimplementaremos para descargarlo directamente de la red.
 * 
 */

const fs = require('fs');

// const getFlags () => {
//     // devuelve cuántas playas en cada ayuntamiento
// }

const getFlagsForCouncil = (flags, council) => {

    const found = flags
    .slice(1)
    .map(flag => flag.split(';'))
    .filter(flag => flag.length > 2)
    .filter(flag => flag[2].toLowerCase().includes(council.toLowerCase()))

    return found.length

}

const bandeiras = fs.readFileSync(`${__dirname}/../resources/bandeiras_azuis_2019.csv`, 'latin1').split('\r\n')

let concelloABuscar = 'coru'

//let concellos = bandeiras.filter(conceBan => getFlags(conceBan, concelloABuscar) === concelloABuscar)

console.log(getFlagsForCouncil(bandeiras, concelloABuscar));


//console.log(concellos);

//const count getFlagsForCouncil(bandeiras, 'Vigo')

//console.log('Banderas de 2019: ', bandeiras)