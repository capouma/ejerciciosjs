/**
 * Funciones
 * 
 * Crea una función que reciba un array y un número y 
 * devuelva cuántos números del array son mayores que 
 * el indicado por parámetro
 * 
 * 
 */
function getHigherThan(numeros, umbral)
{
    let totalNumeros = 0
    for (numero of numeros)
    {
        if(numero > umbral)
        {
            totalNumeros = totalNumeros + 1
        }
    }
    return totalNumeros 
}


const threshold = 0;
const values = [1,2,3,4,5,6,7,8];
const counter = getHigherThan(values, threshold);

console.log(`El número de valores mayores que ${threshold} es ${counter}`);