
// crea un nuevo array cuyos elementos sean los de las posiciones
// pares del array original

values = [2, 4, 6, 8, 10]
output = []

for (i =0; i < values.length; i++)
{
    if (i % 2 === 0)
    {
        output.push (values[i])
    }
}

for (salida of output)
{
    console.log(salida);
}