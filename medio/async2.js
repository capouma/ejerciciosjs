/**
 * En los próximos ejercicios veremos cómo hacer peticiones de red,
 * es decir, pedirle a `NodeJS` que descargue de Internet el contenido
 * de una `URL`. Esta descarga no será inmediata, así que estudiaremos
 * un mecanismo para indicarle cuál es el código que tiene que ejecutar una 
 * vez finalizada.
 * 
 * Antes de eso, implementemos una serie de funciones auxiliares que, de paso,
 * nos sirven de repaso de cómo iterar `arrays`, bien con `for` tradicional
 * o con `map/filter`
 * 
 * El ejercicio consiste en implementar dos funciones que realizan estadísticas
 * sobre datos reales: las banderas azules en ayuntamientos gallegos. En esta
 * primera aproximación leeremos los datos de disco. En ejercicios posteriores
 * lo reimplementaremos para descargarlo directamente de la red.
 * 
 */
 const fs = require('fs');
const axios = require('axios');

const getFlagsForCouncil = (flags, council) => {

    const beaches = flags
        .slice(1)             // elimino cabecera
        .map(line => line.split(';'))     // convierto cada línea en un array
        .filter(campos => campos.length > 1)    // me quedo con los que tengan datos (líneas no vacías)
        .filter(campos => campos[2].toLowerCase() === council.toLowerCase())  // me quedo con los del ayuntamiento que me piden

    return beaches.length;
}

const bandeiras = fs.readFileSync(`${__dirname}/../resources/links.csv`, 'latin1').split('\n')
let ganador

let banderaAnho = bandeiras
    .map(line => line.split(','))
    .filter(campos => campos.length > 1)
    .map(campos => ControlPorAnhos(campos[0],campos[1]))

console.log('línea al final')

function ControlPorAnhos(anho,link)
{
    let ganador
    axios
        .get(link)
        .then(response => {
            const flags = response.data.split('\r\n');
            const counter = getFlagsForCouncil(flags, 'vigo');
            console.log(`Año: ${anho} Counter: ${counter}`);
        });
}
