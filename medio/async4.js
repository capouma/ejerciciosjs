/** 
 * Lee el fihero nacional_covid19.csv y crea funciones para lo siguiente
 * (Fuente: https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/nacional_covid19.csv    )
 * 
 * 1) buscar el día en el que hubo mayor incremento en el número de contagios
 * 
 * 2) generar un nuevo array con el porcentaje de nuevos contagios respecto al total de contagios del 
 * día anterior. Ejemplo. Si el día 1 hay 1000 contagios, y el día 2 hay 1200, el incremento ha sido del
 * 20% ((1200 - 1000) / 1000)*100
 * 
 * 3) escribir en pantalla un resumen para el día indicado en los parámetros de la función:
 *         printSummary(day, month, year)
 * 
 * debe pintar en pantalla
 *       ******************************
 *       * Contagios totales: xx      *
 *       * Altas totales: xx          *  
 *       * Incremento contagios: 20%  *
 *       * Fallecidos: xx             *
 *       * UCI: xx                    *
 *       * Hospitalizados: xx         * 
 *       ******************************
 * 
 * Si no existe algunos de los datos, no debe aparecer la línea correspondiente
 * 
 * 
 * 4) calcular cuántos nuevos contagios hay en cada día de la semana. La salida de la función 
 * será un objeto con las claves 'lunes', 'martes',etc. y los valores serán el número
 * de nuevos contagios que ha habido en dichos días
 * 
 * 5) El número de nuevos contagios puede variar mucho de un día a otro por diversos motivos. Para
 * que la interpretación de las gráficas sea más sencilla se suelen suavizar los datos. ¿Cómo? Una de
 * las técnicas es hacer una media de los últimos días. El objetivo de este apartado es crear un array
 * con los nuevos contagios "suavizados" con la media de los últimos tres días. Es decir, el día 3 contendrá
 * la suma de los contagios de los días 1, 2 y 3 dividida por 3.
 * 
 */

 function limpiarcsv(csv) {
    const datos = csv.data
        .split('\n')
        .slice(1)
        .filter(campos => campos.length > 1)
        return datos
}

function CalcularMayorIncremento(csvLimpioDescompuesto)
{
    let mayorIncremento = 0;
    let diaMayorIncremento = 0;

    for (let i = 1; i < (csvLimpioDescompuesto.length); i++)
    {
        //Calcular fecha con incremento mayor
        if (parseInt(csvLimpioDescompuesto[i][2]) - parseInt(csvLimpioDescompuesto[i - 1][2]) >= mayorIncremento)
        {
            mayorIncremento = parseInt(csvLimpioDescompuesto[i][2]) - parseInt(csvLimpioDescompuesto[i - 1][2])
            diaMayorIncremento = csvLimpioDescompuesto[i][0];
        }
    }
    console.log(mayorIncremento);
    return diaMayorIncremento
}

function calcularPorcentajeNuevosContagios(csvLimpioDescompuesto)
{
    let porcentajeNuevosContagios = []

    for (let i = 1; i < (csvLimpioDescompuesto.length); i++)
    {
        //Calculo de porcentaje de nuevos contagios

        let fechaYPorcenNuevosContagios ={
            fecha: csvLimpioDescompuesto[i][0],
            porcentaje: `${parseInt((parseInt(csvLimpioDescompuesto[i][2]) - parseInt(csvLimpioDescompuesto[i - 1][2])) / csvLimpioDescompuesto[i - 1][2] * 100)}%`
       };
    
        porcentajeNuevosContagios.push(fechaYPorcenNuevosContagios);
    }
    return porcentajeNuevosContagios
}

function rellenarDatosCuadroPantalla (csvLimpioDescompuesto, fechaABuscar , nombre, filaABuscar)
{

    const buscaDatosEnLinea =csvLimpioDescompuesto
    .filter(buscalinea => fechaABuscar === buscalinea[0])
    .map(buscarCasosTotales => buscarCasosTotales[filaABuscar])

    let cuadroResumen =
    {
        nombre: nombre,
        cantidad: buscaDatosEnLinea[0],
        tieneDatos: buscaDatosEnLinea[0] > 0
    };
    return cuadroResumen
}

function rellenarDatosCuadroPantallaPorcentaje(porcentajeNuevosContagios, fechaABuscar) {
    const incrementoContagios = porcentajeNuevosContagios
        .filter(buscalinea => fechaABuscar === buscalinea.fecha)
        .map(buscaPorcentaje => buscaPorcentaje.porcentaje);

    let cuadroResumen = {
        nombre: 'Incremento contagios: ',
        cantidad: incrementoContagios[0],
        tieneDatos: incrementoContagios[0] !== '%'
    };
    return cuadroResumen;
}

function imprimirSumario (dia, mes, anho, csvLimpioDescompuesto,porcentajeNuevosContagios)
{
    let fechaABuscar = `${anho}-${mes}-${dia}`

    let cuadroResumenPantalla = []

    cuadroResumenPantalla.push(rellenarDatosCuadroPantalla(csvLimpioDescompuesto, fechaABuscar, 'Contagios totales: ', 1))
    cuadroResumenPantalla.push(rellenarDatosCuadroPantalla(csvLimpioDescompuesto, fechaABuscar, 'Altas totales: ', 4))
    cuadroResumenPantalla.push(rellenarDatosCuadroPantallaPorcentaje(porcentajeNuevosContagios, fechaABuscar))
    cuadroResumenPantalla.push(rellenarDatosCuadroPantalla(csvLimpioDescompuesto, fechaABuscar, 'Fallecidos: ', 5))
    cuadroResumenPantalla.push(rellenarDatosCuadroPantalla(csvLimpioDescompuesto, fechaABuscar, 'UCI: ', 6))
    cuadroResumenPantalla.push(rellenarDatosCuadroPantalla(csvLimpioDescompuesto, fechaABuscar, 'Hospitalizados: ', 7))

    return cuadroResumenPantalla = cuadroResumenPantalla.filter(conDatos => conDatos.tieneDatos === true)
}

function calcularGraficaDiaria(descompuesto)
{
    let grafica = {};

    for (let i = 0; i < descompuesto.length; i++) {
        let diaAComprobar = new Date(descompuesto[i][0]);

        if (grafica[diaAComprobar.getDay()] !== undefined) {
            grafica[diaAComprobar.getDay()] = descompuesto[i][2];
        }

        else {
            grafica[diaAComprobar.getDay()] += descompuesto[i][2];
        }
    }
    return grafica;
}

function suavizaDatosTresDias(descompuesto) {
    let cadaTresDias = [];
    for (let i = 0; i < (descompuesto.length - 3); i++) {
        cadaTresDias.push(parseInt((parseInt(descompuesto[i][2]) + parseInt(descompuesto[i + 1][2]) + parseInt(descompuesto[i + 2][2])) / 3));
    }
    return cadaTresDias;
}

 const axios = require('axios');

 const URL = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/nacional_covid19.csv';


 axios
    .get(URL)
    .then(csv =>
        {
            const csvLimpio = limpiarcsv(csv)
            const descompuesto = csvLimpio.map(descompone => descompone.split(','));

            const fechaMayorIncremento = CalcularMayorIncremento(descompuesto);
            const porcentajeNuevosContagios = calcularPorcentajeNuevosContagios(descompuesto);

            console.log(fechaMayorIncremento);
            console.log(porcentajeNuevosContagios);

            let cuadroResumenPantalla = imprimirSumario('25', '02', '2020', descompuesto, porcentajeNuevosContagios)

            console.log('*'.repeat(30));
            // for(let i= 0; i < cuadroResumenPantalla.length; i++)
            // {
            //     let espaciosBlanco = 30 - (cuadroResumenPantalla[i].nombre + cuadroResumenPantalla[i].cantidad).length
            //     console.log(`* ${cuadroResumenPantalla[i].nombre}${cuadroResumenPantalla[i].cantidad}${' '.repeat(espaciosBlanco-3)}*`);
            // }
            // console.log('*'.repeat(30));

            console.log(calcularGraficaDiaria(descompuesto));
            
            console.log(suavizaDatosTresDias(descompuesto));

        }
    );








