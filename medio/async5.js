





function limpiarcsv(csv)
{
    const datos = csv.data
        .split('\n')
        .slice(1)
        .filter(campos => campos.length > 1)
    return datos
}

function recalcularTiempos(tiempos) {
    let segTotal = (tiempos['horas'] * 3600) + (tiempos['minutos'] * 60) + tiempos['segundos']

    tiempos['horas'] = Math.floor(segTotal / 3600)
    tiempos['minutos'] = Math.floor(segTotal % 3600 / 60)
    tiempos['segundos'] = Math.floor(segTotal % 3600 % 60)
    
    return tiempos
}

function calcularTiempos(descompuesto, nombre)
{
    let tiempos = {
        nombre: '',
        horas: 0,
        minutos: 0,
        segundos: 0
    };


    for (let i = 0; i < descompuesto.length; i++)
    {
        let hora = 0
        let minuto = 0
        let segundo = 0

        if (descompuesto[i][2] === nombre)
        {

            if (descompuesto[i][4].trim().length >0)
            {
                [hora, minuto, segundo] = descompuesto[i][4]
                .split(' ')
                .map(limpiarCaracteres => limpiarCaracteres.replace(/'/g, '').replace(/h/g, ''))
            }
            tiempos['nombre'] = descompuesto[i][2];
            tiempos['horas'] += parseInt(hora)
            tiempos['minutos'] += parseInt(minuto)
            tiempos['segundos'] += parseInt(segundo)
        }
    }

    return recalcularTiempos(tiempos);
}

const axios = require('axios');

const URL = 'https://raw.githubusercontent.com/camminady/LeTourDataSet/master/Riders.csv';


axios
.get(URL)
.then(csv =>
    {
        const csvLimpio = limpiarcsv(csv)
        const descompuesto = csvLimpio.map(descompone => descompone.split(','));


        console.log(calcularTiempos(descompuesto, 'LUCIEN POTHIER'));
    }
);