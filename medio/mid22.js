// Objetivo: array con nombre completo de los alumnos aprobados

const students = [
{
    grade: 4,
    name: 'Manolo',
    surname: 'Suárez'
},
{
    grade: 9,
    name: 'Andrea',
    surname: 'Suárez'
},
{
    grade: 7,
    name: 'Paca',
    surname: 'Costas'
},
]

const aprobados = nota => nota.grade >=5
const nombresCompletos = nombre => `${nombre.name} ${nombre.surname}`

const alumnosAprobados = students
    .filter(aprobados)
    .map (nombresCompletos)

console.log(alumnosAprobados);