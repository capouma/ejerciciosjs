//calcula la media de los elementos que sean mayores o iguales que 5;

grades = [7.9, 9, 1, 2];
notaMedia = 0;
numNotas = 0;
for (grade of grades) 
{
    if (grade >= 5)
    {
        notaMedia = notaMedia + grade;
        numNotas = numNotas + 1;
    }
}

notaMedia = notaMedia / numNotas;

console.log(`La nota media es: ${notaMedia}`);