/**
 * Elimina los duplicados
 * 
 * Ejemplo:
 *   - Array entrada: [1,2, 2, 3, 4 ,4, 1]
 *   - Array salida:  [1,2,3,4]
 * 
 */

input = [1,2, 2, 3, 4 ,4, 1];


// output = [...new Set (input)];

// console.log(output);

output = [input[0]]

for (i = 0; i < input.length; i++)
{
    if (output.includes(input[i])=== false)
    {
        output.push(input[i]);
    }
}

console.log(output);