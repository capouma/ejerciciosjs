
//Funciones
function extraerHashtags(tweet) {

    let hashtag = tweet
    .split (' ')
    .filter(cadena => cadena.indexOf('#') !== -1)

    return hashtag;
}

//Variables
tweets = ['aprendiendo #javascript', 'empezando el segundo módulo del bootcamp!', 'hack a boss bootcamp vigo #javascript hola #codinglive']

let hashtag = tweets.map(tweet => extraerHashtags(tweet))

console.log(hashtag);