const status = [
    [1, 1, 0],
    [0, 1, 0],
    [0, 0, 1]
]

// 0 -> no hay ficha
// 1 -> ficha del jugador uno
// 2 -> ficha del jugador dos

checkWinner(status)

function checkWinner(status) {
    // devuelve 0 si aún no hay ganador
    // devuelve 1 si gana el uno
    // devuelve 2 si gana el dos

    let p = []
    let jugador= [1,2]
    let salir = false
    let ganador = 0
    for (let j = 0; j < jugador.length; j++)
    {
        //Comprueba si hay ganador por fila
        if (salir === false)
        {
            for (let i = 0; i < status.length; i++)
            {
                let fila = status[i].filter (x => x === jugador[j])
        
                if (fila.length < status[i].length)
                {
                    console.log(`el jugador ${jugador[j]} no gana en fila ${i}`);
                }
                else
                {
                    console.log(`GANADOR JUGADOR: ${jugador[j]}`);
                    salir = true
                    ganador = jugador[j]
                    break
                }
            }
        
            //Comprueba si hay ganador por columna
            if (salir === false)
            {
                let total = 0

                while (total < status.length)
                {
                    let columnas = []
                    for (let i = 0; i < status.length; i++)
                    {
                        columnas.push(status[i][total])
                    }
                    
                    let comprueba = columnas.filter(iguales => iguales === jugador[j])
                    if (comprueba.length < status.length)
                    {
                        console.log('SIGUE BUSCANDO');
                    }
                    else
                    {
                        console.log('PREMIO');
                        salir = true
                        break
                    }
                    total += 1
                }
            }

            //diagonal creciente
            if (salir === false)
            {
                let acumula = []
                for (let i = 0; i < status.length; i++)
                {
                    acumula.push(status[i][i])
                }
                let diagonalCre = acumula.filter(gana => gana === 1)

                if (diagonalCre.length < status.length)
                {
                    console.log(`sigue buscando`);
                }
                else
                {
                    console.log(`GANADOR JUGADOR!`);
                    salir = true
                }
            }

            //diagonal decreciente
            if (salir === false)
            {
                let acumulados =[]
                for (let i = (status.length - 1); i >= 0; i--)
                {
                    acumulados.push(status[i][i])
                }
                let diagonalCre = acumulados.filter(gana => gana === 1)

                if (diagonalCre.length < status.length)
                {
                    console.log(`sigue buscando`);
                }
                else
                {
                    console.log(`GANADOR JUGADOR`);
                    salir = true
                }
            }
        }   

    }

}
