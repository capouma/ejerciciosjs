/**
 * Busca el ISBN de cinco libros (en Amazon, en Casa del LIbro, etc)
 * y usa la siguiente URL para descargar información sobre el mismo.
 * 
 * https://openlibrary.org/api/books?bibkeys=ISBN:9788401328510&jscmd=data&format=json
 * 
 * Finalmente, obtén una lista con los títulos [ordenados por año de publicación]
 * 
 */

// El señor de los anillos 9788445073728
// La caida de Gondolin 9788445006092
// Beren y Luthien 9788445005064
// El nombre del viento 9788499082479
// La sangre de los elfos 9788498890457

const axios = require('axios');

const libros = ['9788445073728', '9788445006092', '9788445005064', '9788499082479', '9788498890457']

let librosUrl = libros.map(isbn => `https://openlibrary.org/api/books?bibkeys=ISBN:${isbn}&jscmd=data&format=json`)

let promesas = librosUrl.map(promesa => axios.get(promesa))



Promise.all(promesas)
.then( data => {

    let datosLibro =[]

    for (let i = 0; i < libros.length; i++)
    {
        let libro ={
             titulo: data[i].data[`ISBN:${libros[i]}`].title,
             anhoPublicacion: parseInt(data[i].data[`ISBN:${libros[i]}`].publish_date)
        };
        datosLibro.push(libro)

    }

    datosLibro.sort(function(a, b) {
        return a.anhoPublicacion - b.anhoPublicacion;
    });

    console.log(datosLibro);

});