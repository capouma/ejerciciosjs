/**
 * Arrays y funciones
 * 
 * Vamos a analizar cómo avanza una enfermedad infecciosa.
 * 
 * Para ello representamos en un array la población de 
 * una determinada zona. Hay un 1 si en esa posición del array
 * la persona está infectada, y un cero si no lo está. Ej.:
 * 
 *    [0,0,0,0,1,0,1,1,0]
 * 
 * Teniendo en cuenta que cada persona contagiada infecta
 * a los dos que están a su lado cada día, ¿cuántos días
 * tarda en infectarse toda la población?
 * 
 * Ejemplo de evolución:
 *    Day 0: [0,0,0,0,1,0,1,1,0]
 *    Day 1: [0,0,0,1,1,1,1,1,1]
 *    Day 2: [0,0,1,1,1,1,1,1,1]
 *    Day 3: [0,1,1,1,1,1,1,1,1] 
 *    Day 4: [1,1,1,1,1,1,1,1,1] 
 * 
 * Nota: ¿cómo copiar un array?
 * No lo hemos dado aún, pero para este ejercicio os puede ser útil saber
 * que para copiar un array no vale con hacer:
 *    variableArray2 = variableArray1
 * 
 * como haríamos con los números. En su lugar haremos lo siguiente:
 *    variableArray2 = [...variableArray1]
 * 
 */


function infectados(poblacion) {
    let personasInfectadas = [];

    for (i = 0; i < poblacion.length; i++) {

        if (poblacion[i] === 0 && poblacion[i + 1] === 1) {
            console.log(`enfermo nuevo en i ${i}`);
            personasInfectadas.push(1);
        }
        else if (poblacion[i] === 1) {
            if (poblacion[i + 1] === 1) {
                console.log(`enfermo nuevo en i ${i}`);
                personasInfectadas.push(1);
            }

            else {
                console.log(`enfermo nuevo en i ${i}`);
                personasInfectadas.push(1);+
                console.log(`enfermo nuevo en i+1 ${i}`);
                personasInfectadas.push(1);
                i++;
            }

        }
        else if (poblacion[i] === 0 && poblacion[i + 1] === 0) {
            console.log(`NO enfermo en i ${i}`);
            personasInfectadas.push(0);
        }
    }
    return personasInfectadas
}

function controlDeSanos(poblacion) {
    let controlSanos = 0

    for(sanos of poblacion)
    {
        if (sanos === 0)
        {
            controlSanos++
        }
    }
    if (controlSanos === poblacion.length)
    {
        return true
    }
    else
    {
        return false
    }
}

let variantesVirus1 = [0,0,0,0,1,0,1,1,0]
let variantesVirus2 = [0,0,0,0,0,0,0,0,0]
let variantesVirus3 = [1,1,1,1,1,1,1,1,1]

let variantesVirus = [...variantesVirus1]

let dias = 0

if (!controlDeSanos(variantesVirus))
{
    while (variantesVirus.includes(0))
    {
        dias++
        console.log(`dia: ${dias}`);
        variantesVirus = [...infectados(variantesVirus)]
    }

    console.log(`dias totales para la infeccion: ${dias}; poblacion infectada ${variantesVirus}`);    
}
else
{
    console.log(`Población sana sanota y no hay infectados`);
}