

name = 'manolo';
surname1 = 'Pérez';
surname2 = 'Gómez';

grade1 = 1;
grade2 = 7;

nombreCompleto = ` ${name} ${surname1} ${surname2} `;
nombreCompletoLength = nombreCompleto.length;
media = ((grade1 + grade2) / 2).toFixed(1);

//Control de posicion Inicial de media
posicionMediaIniCal = ((nombreCompletoLength - media.length) / 2).toFixed(0);

//Control de calificación
if (media >= 5)
{
    nota = '(APROBADO)';
}
else
{
    nota = '(SUSPENSO)';
}

//Control posicion Inicial nota
posicionMediaIniNota = ((nombreCompletoLength - nota.length) / 2).toFixed(0);

//Control de posicion Fin de media
if(media.length >3)
{
    posicionMediaFinCal = posicionMediaIniCal;
}
else
{
    posicionMediaFinCal = posicionMediaIniCal-1;
}

console.log('*'.repeat(nombreCompletoLength + 2));
console.log(`*${nombreCompleto}*`);
console.log('*' + ' '.repeat(nombreCompletoLength) + '*');
console.log('*' + ' '.repeat(posicionMediaIniCal) + media + ' '.repeat(posicionMediaFinCal) + '*');
console.log('*' + ' '.repeat(posicionMediaIniNota) + nota + ' '.repeat(posicionMediaIniNota) + '*');
console.log('*'.repeat(nombreCompletoLength + 2));