/**
 * Dado un array de números devuelve un nuevo 
 * array donde cada elemento se haya desplazado 
 * una posición hacia la derecha.
 * 
 * Ejemplo:
 * 
 * Array original:   [1,2,3,4]
 * 
 * Array resultante: [4,1,2,3] 
 * 
 * Nota cómo se trata de un desplazamiento circular,
 * donde el último elemento pasa a ser el primero
 */

 values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
 output = [];

 avances= 4;

 for (value of values)
 {
     output.push(value);
 }

for (i = 0; i < avances; i++)
{
    output.unshift(output[(output.length-1)]);
    output.pop()
}
console.log(output);
console.log(values);