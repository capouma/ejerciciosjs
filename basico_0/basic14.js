//Escribe en pantalla la generación a la que pertenece
//una persona nacida en el año indicado en la variable
//"birthYear"

//Variables
birthYear = 1969;
edadActual = 39

//Logica
if (birthYear < 1949)
{
    generacion = 'QUE ES ESO DE GENERACION?';
}

else if ((birthYear >1948) && (birthYear< 1969))
{
    generacion = 'BABY BOOM';
}
else if ((birthYear >1968) && (birthYear< 1981))
{
    generacion = 'GENERACION X';
}
else if ((birthYear >1980) && (birthYear< 1995))
{
    generacion = 'MILLENIALS';
}
else if (birthYear >= 1995)
{
    generacion = 'Z';
}

console.log(`ERES DE LA GENERACION: ${generacion}`);