
//VARIABLES
salarioBruto = 30000;

//Tramos de pago
tramo1 = 12450;
tramo2 = 20200;
tramo3 = 35200;
tramo4 = 60000;

//Retencion total por tramo
retencionTramo1 = tramo1 * 0.19;
retencionTramo2 = (tramo2 - tramo1) * 0.24;
retencionTramo3 = (tramo3 - tramo2) * 0.30;
retencionTramo4 = (tramo4 - tramo3) * 0.37;
retencionTramo5 = (tramo4 - tramo4) * 0.45;


if (salarioBruto <= tramo1)
{
    aPagar = salarioBruto * 0.19;
    Tramo = 'Tramo1'
}
else if (salarioBruto <= tramo2)
{
    retencionTramo2 = (salarioBruto - tramo1) * 0.24;
    aPagar = retencionTramo1 + retencionTramo2;
    Tramo = 'Tramo2'
}
else if (salarioBruto <= tramo3)
{
    retencionTramo3 = (salarioBruto - tramo2) * 0.30;
    aPagar = retencionTramo1 + retencionTramo2 + retencionTramo3;
    Tramo = 'Tramo3'
}
else if (salarioBruto <= tramo4)
{
    retencionTramo4 = (salarioBruto - tramo3) * 0.37;
    aPagar = retencionTramo1 + retencionTramo2 + retencionTramo3 + retencionTramo4;
    Tramo = 'Tramo4'
}
else if (salarioBruto > 60000)
{
    retencionTramo5 = (salarioBruto - 60000) * 0.45;
    aPagar = retencionTramo1 + retencionTramo2 + retencionTramo3 + salarioTramo4 + retencionTramo5;
    Tramo = 'Tramo5'
}

console.log(`EL tramo actual es ${Tramo} y hay que pagar ${aPagar}`);