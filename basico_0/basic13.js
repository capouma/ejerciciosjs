//Nota media de 3 notas de un alumno

//Variables
grade1 = 10;
grade2 = 10;
grade3 = 10;

//Calculo nota media
notaMedia = (grade1 + grade2 + grade3) / 3

//Condición
if (notaMedia < 5)
{
    notaFinal = 'SUSPENSO'
}
if ((notaMedia >= 5) && (notaMedia < 6))
{
    notaFinal = 'APROBADO'
}
if ((notaMedia >= 6) && (notaMedia < 7))
{
    notaFinal = 'BIEN'
}
if ((notaMedia >= 7) && (notaMedia < 9))
{
    notaFinal = 'NOTABLE'
}

if ((notaMedia >= 9) && (notaMedia <= 10))
{
    notaFinal = 'SOBRESALIENTE'
}

//Salida en pantalla
console.log(`La nota media es de: ${notaFinal}`);